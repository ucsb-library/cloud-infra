(async function () {

  const fetch = require('node-fetch')
  const lux = require('luxon')

  const API_KEY = process.env.ELASTIC_API_KEY
  const API_HOST = process.env.ELASTIC_HOST
  const API_PORT = process.env.ELASTIC_PORT

  const PAST_DUE = process.env.PAST_DUE || '60'

  const url_base = `http://${API_HOST}:${API_PORT}`
  const get_options = {
    headers: {
      "Authorization": `ApiKey ${API_KEY}`
    }
  }

  const response = await fetch(`${url_base}/fluentd*`, get_options)
  const indices = JSON.parse(await response.text())

  const old = Object.keys(indices).filter( value => {
    let parsed
    let extract

    try {
      // default 20240101 format

      extract = value.match(/[0-9]{8}$/)[0]
      parsed = lux.DateTime.fromISO(extract)
    } catch {
      try {
        // logstash 2024.01.01 format
        extract = value.match(/[0-9]{4}\.[0-9]{2}\.[0-9]{2}$/)[0]
        parsed = lux.DateTime.fromFormat(extract, 'yyyy.MM.dd')
      } catch {
        console.log(`couldn't parse ${value}`)
        return false
      }
    }

    const cutoff = lux.DateTime.now().minus({ days: PAST_DUE })
    // console.log(`comparing ${parsed} to ${cutoff}`)
    return parsed < cutoff
  })

  // console.dir(old)

  const del_options = {
    method: 'DELETE',
    headers: {
      "Authorization": `ApiKey ${API_KEY}`
    }
  }
  old.forEach(async function(value, _index, _array) {
    console.log(`deleting index ${value}`)
    const response = await fetch(`${url_base}/${value}`, del_options)
    console.log(await response.text())
  })
})()
