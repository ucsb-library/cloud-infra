# UCSB Libraries Cloud Infrastructure

Documentation, manifests, scripts, and issue tracking for UCSB Libraries cloud
infrastructure.

## Kubernetes/EKS

Most of DLD's active projects run as containerized workloads ad deployed in a
[Kubernetes][k8s] cluster managed in [AWS EKS][eks]. This cluster hosts
production workloads, staging, sandbox, and test apps, as well as DLD and
Surfliner DevOps pipeline runners and bots. Documentation for this cluster
is split into:

  - [EKS User Documentation][eks-user-docs]: for information about deploying and
    supporting workloads in the cluster;
  - [EKS Cluster Maintenance][eks-maintenance]: for cluster configuration details
    and maintenance tasks.

## Elasticsearch, Fluentd & Kibana

Logs across the cluster are collected by [Fluentd][fluentd] and made available via
Elasticsearch/Kibana. Documentation and configuration for this setup is maintained
in [`./efk`][efk].

## [Google Services][google]

We use Google functional accounts and Google Groups for email integration, and
use Google API services for managing authentication.

[efk]: efk/README.md
[eks]: https://aws.amazon.com/eks/
[eks-maintenance]: eks/maintenance.md
[eks-user-docs]: eks/documentation.md
[fluentd]: https://www.fluentd.org/
[google]: google/README.md
[k8s]: https://kubernetes.io/
