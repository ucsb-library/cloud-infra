# Documentation of EFK-Stack

## Local Access and Search

Logs are viewed from the Analytics > Discover tab. There we will see all the
logs captured from the cluster with their metadata separated into different
fields.

A few point to note:
1. We can change the time from the top right options. This could be useful if we want to examine logs from a certain time period.
2. We can use the search box on the top left to filter out logs with different values. For example, if we want to see only the cylinder logs from past two days, we can search
    ```
    kubernetes.namespace_name: "cylinders-review" or kubernetes.namespace_name: "cylinders-prod" or kubernetes.namespace_name: "cylinders-staging"
    ```

    or if we want to see recent errors from our production Solr instances:
    ```
    kubernetes.namespace_name : "solr-prod" and log : *error*
    ```

(See the [KQL documentation](https://www.elastic.co/guide/en/kibana/7.17/kuery-query.html) for more on the search syntax.)

## Configuring the EFK Stack

Fluentd helm chart installs two configmaps, `fluentd-aggregator-cm` and
`fluentd-forwarder-cm`. With the original values of the helm, the later one
forward all the logs to `fluentd-aggregator-cm`. But we want it to forward the
logs to Elasticsearch instead, so we create values.yaml files to do that. The
source block in the `fluentd.yaml` captures the logs from the containers, and
send them to Elasticsearch for storage.

Priority Class is also changed and set to `dld-critical` for the fluentd pods.

**Note:** Since the elasticsearch pod and the fluentd pods are hosted in the
same namespace, specificying the host name as `bitnami-elastic-elasticsearch` is
enough.

### Fluentd yaml file
    <match fluent.**>
      @type null
    </match>

This match blocks ignore the logs (/events) of the fluentd pods. We perfer not
to inlcude the logs coming from the fluentd to increase the load on EFK stack.

The source block tells us where all the data is coming from. Fluentd standard
input plugins that we are using here are: *@http* and *@tail*. The http provides
an HTTP endpoint to accept incoming HTTP messages whereas the tail plugin allows
Fluentd to read events from the tail of text files. We may add multiple source
configurations as required.

HTTP input for the liveness and readiness probes.
```
<source>
  @type http
  port 9880
</source>
```

Get the logs from the containers running in the node
```
<source>
  @type tail
  path /var/log/containers/*.log
  exclude_path /var/log/containers/*fluentd*.log
  exclude_path /var/log/containers/*kibana*.log
  pos_file /opt/bitnami/fluentd/logs/buffers/fluentd-docker.pos
  format json
  keep_time_key true
  key_name log
  read_from_head true
  tag kubernetes.*
  time_key time
</source>
```

This source block does the trick of collecting the logs from the all the
containers in the Cluster.

First we declare the type of the *input plugin*.
```
@type tail
path /var/log/containers/*.log
```

The first line of code delcares our tail plugin needed to collect logs.  The
second line tells the fluentd where to collect the logs from. In our case, the
logs of the containers are being stored in the */var/log/containers/*, and *
means "everything in the next directory", and ".log" indicates the extension of
the log files. In other words, this line tells fluentd to collect the logs from
logs file under the path */var/log/containers/*.

```
exclude_path /var/log/containers/*fluentd*.log
exclude_path /var/log/containers/*kibana*.log
```

The above two lines tells fluentd to exclude the .log file who have *fluentd* or
*kibana* in their name.

```
pos_file /opt/bitnami/fluentd/logs/buffers/fluentd-docker.pos
```

When fluentd runs this line of code, it creates a new file of the name
"fluentd-docker.pos" under the path */opt/bitnami/fluentd/logs/buffers*. This
new position file will record the last log (/position of the logs) in the log
files. This is method used by Fluentd to track logs processed so far.

Suppose for some reason, our fluentd breaks and stop collecting logs. After
debugging the issue and starting fluentd back up, there will remain some logs
who were not processed (or collected) by fluentd, so instead of starting the
process of collecting logs from the first line of the log file, it will start
collecting logs after the position of the last log it collected before it
crashed.

```
tag kubernetes.*
```

This line of code tags the kubernetes metadata into fluentd.

```
read_from_head true
format json
time_key time
keep_time_key true
key_name log
```

The first line from this code block tells fluentd to start reading the logs from
the head of the file or the last read position recorded in pos_file, not
tail. This to work efficiently, we need to have pos_file declared before
implementing *read_from_head*. If this is false, fluentd will read the logs from
the first line of the log file, and make the process computionally expensive and
longer.

*format json* chooses the format of the logs while ouputting them. In other
words, it converts the an event to json. This can be replaced with a parser to
output the logs in a different format.

An example of a parser can look like
```
<filter **>
  @type parser
  key_name log
  <parse>
    @type multi_format
    <pattern>
      format json
      time_key time
      keep_time_key true
    </pattern>
  </parse>
</filter>
```

This code block can be placed below the source block. The double astrek in
*<filter $**$>* just means that take every log entry coming from the source
block.

For our cluster, *@apache2* seems like a promising parser. I used the *@apache2*
parser and the field that containes the log entries (called "log" in Kibana) was
split into multiple fields correctly, but there was no data attached to these
new fields. This was the farthest I was able to go.

*time_key* indicates if we would like to specify time field for event time. Most
of the events have this time field, but if for some reason, an event do not have
this time field, then current time is used.

*keep_time_key* indicates if it true, keep time field in the records.

*key_name* specifies the field name in the record to parse. We are specifying
the "log" field from the kubernetes metadata here.

```
# enrich with kubernetes metadata
<filter kubernetes.**>
  @type kubernetes_metadata
  skip_labels true
</filter>
```

This filter block enrich the tag field (that we declared earlier) with
kubernetes metadata. `skip_labels` is used to avoid a [parsing
issue](https://github.com/uken/fluent-plugin-elasticsearch/issues/787) with the
`kubernetes.label.app` field.

Now we look at the ouput config block in the Fluentd yaml file.
```
 # Throw the healthcheck to the standard output instead of forwarding it
<match fluentd.healthcheck>
    @type null
</match>
```

This throws away the healthcheck of fluentd pods instead of forwarding it. The
log events from fluend pods take up a lot of space in storage than other log
events, so it is a good practice to not include the fluentd events and their
healthchecks.

```
# Forward all logs to the elasticsearch (insetead of aggregators)
<match **>
  @type elasticsearch
  host "#{ENV['ELASTICSEARCH_HOST']}"
  port "#{ENV['ELASTICSEARCH_MASTER_SERVICE_PORT']}"
  index_name fluentd.%Y%m%d
  include_tag_key true
  log_es_400_reason true
  <buffer>
    flush_interval 5s
    flush_mode interval
    flush_thread_count 1
    overflow_action drop_oldest_chunk
  </buffer>
</match>
```

The match block takes the log events coming from the source blocks and tells
fluentd what to do with them. The most common use of the match directive is to
output events to other systems, in our cases (Elasticsearch). Only events with a
tag matching the pattern will be sent to the output destination, as in our case,
it is the kubernetes metadata.

Each match directive must include a match pattern and a @type parameter. The
match patter is *<match $**$>* The double astreak means that we will take every
log coming from the source block. If we wish to only take specific log events,
then we need to change the match patter to something like:

```
<match kubernetes/var/log/containers/**cylinder**.log>
```

The above code cell is an example of a match patter which only takes the log
events from containers who have *cylinder* keyword in their name.  The issue
with this that one match block only creates one index-name, so we will need
mutiple match blocks for each container.

```
@type elasticsearch
elasticsearch-master
port 9200
```

The above code cell tells the fluentd where to send the captured logs. In our
case, it is Elasticsearch.

```
index_name fluentd.%Y%m%d
include_tag_key true
```

This block specifies the name of the index that we will be creating in
ElasticSearch. It is variable according to the date the logs are aggregated, so
that we don’t end up hitting the maximum number of documents in a single index
(around 2 billion, but you’d be surprised).

```
<buffer>
  flush_interval 5s
  flush_mode interval
  flush_thread_count 1
  overflow_action drop_oldest_chunk
</buffer>
```

The above code cell is self-explanatory. It buffer the logs events. If logging
is interrupted or falls behind, `drop_oldest_chunk` will ensure that we don’t
fail to collect new logs, by abandoning the oldest ones.

This finishes the explaination of Fluentd yaml file.

### Configuring Kibana

In Kibana, go to Stack Management > Data Views > Create new

The basic views to be created will be ones such as ’all’ (`fluentd*`) and
perhaps all production logs (`fluentd.prod*`).  Select "time" as the Timestamp
field. This time field here is the result of declaring *time_key time* in the
source block.

## Important note for Elasticsearch

After a while, the storage space on Elasticsearch will run and it will stop
storing logs. When that happens, along with restarting Elasticsearch pods, we
also need to delete and reinstall the pvc under the EFK-stack namespace.
