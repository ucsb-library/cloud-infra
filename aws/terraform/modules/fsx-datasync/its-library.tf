##
# setup bucket policies and attach to IAM Role
data "aws_iam_role" "datasync_role" {
  provider = aws.source
  name     = var.datasync_role
}

resource "aws_iam_role_policy_attachment" "datasync_bucket_policy" {
  provider   = aws.source
  role       = var.datasync_role
  policy_arn = aws_iam_policy.datasync_bucket_policy.arn
}

resource "aws_iam_policy" "datasync_bucket_policy" {
  provider    = aws.source
  name        = "datasync-bucket-policy-dld-${local.target_name}"
  description = "Policy allowing datasync to use ${data.aws_s3_bucket.target.arn} S3 bucket as a destination."
  policy      = data.aws_iam_policy_document.datasync_bucket_policy.json
}

data "aws_iam_policy_document" "datasync_bucket_policy" {
  statement {
    effect    = "Allow"
    resources = [data.aws_s3_bucket.target.arn]
    actions   = ["s3:GetBucketLocation", "s3:ListBucket", "s3:ListBucketMultipartUploads"]
  }

  statement {
    effect    = "Allow"
    resources = ["${data.aws_s3_bucket.target.arn}/*"]

    actions = [
      "s3:AbortMultipartUpload",
      "s3:DeleteObject",
      "s3:GetObject",
      "s3:ListMultipartUploadParts",
      "s3:PutObject",
      "s3:GetObjectTagging",
      "s3:PutObjectTagging"
    ]
  }
}

##
# setup datasync locations and tasks
resource "aws_datasync_task" "task" {
  provider                 = aws.source
  name                     = "dld-${local.target_name}"
  destination_location_arn = aws_datasync_location_s3.datasync_s3_target_location.arn
  source_location_arn      = aws_datasync_location_fsx_windows_file_system.datasync_fsx_source_location.arn

  options {
    gid                    = "NONE"
    posix_permissions      = "NONE"
    preserve_deleted_files = "REMOVE"
    transfer_mode          = "CHANGED"
    uid                    = "NONE"
    verify_mode            = "ONLY_FILES_TRANSFERRED"
  }

  schedule {
    schedule_expression = var.task_schedule
  }

  excludes {
    filter_type = "SIMPLE_PATTERN"
    value       = var.task_excludes
  }

  includes {
    filter_type = "SIMPLE_PATTERN"
    value       = var.task_includes
  }

  tags = {
    "ucsb:protection-level" = var.uc_protection_level
  }
}

resource "aws_datasync_location_fsx_windows_file_system" "datasync_fsx_source_location" {
  provider           = aws.source
  fsx_filesystem_arn = data.aws_fsx_windows_file_system.source.arn
  domain             = var.fsx_ad_domain
  password           = var.fsx_ad_password
  user               = var.fsx_ad_user
  subdirectory       = var.fsx_filesystem_path

  security_group_arns = [
    data.aws_security_group.fsx_fs_security_group.arn,
    data.aws_security_group.primary_group.arn
  ]

  tags = {
    "ucsb:protection-level" = var.uc_protection_level
  }
}

data "aws_fsx_windows_file_system" "source" {
  provider = aws.source
  id       = var.fsx_filesystem_id
}

resource "aws_datasync_location_s3" "datasync_s3_target_location" {
  provider      = aws.source
  s3_bucket_arn = data.aws_s3_bucket.target.arn
  subdirectory  = ""

  s3_config {
    bucket_access_role_arn = data.aws_iam_role.datasync_role.arn
  }

  depends_on = [
    aws_iam_role_policy_attachment.datasync_bucket_policy
  ]

  tags = {
    "ucsb:protection-level" = var.uc_protection_level
  }
}

data "aws_security_group" "fsx_fs_security_group" {
  provider = aws.source

  tags = {
    "aws:cloudformation:logical-id" = "FSxSecurityGroup",
    "aws:cloudformation:stack-name" = var.fsx_sg_stack
  }
}

data "aws_security_group" "primary_group" {
  provider = aws.source

  tags = {
    "aws:cloudformation:logical-id" = "VpcSecurityGroup",
    "Name" = "LIBRPrimaryVPC Security Group"
  }
}
