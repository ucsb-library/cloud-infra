# fsx-datasync

supports cross account data synchronization from an FSx volume to an S3 bucket.

this module is targeted specifically at syncing content from the FSx filesystems
managed by UCSB ITS for use within the library, but it should be generalizable
for any existing FSx for Windows systems for which an appropriate
ActiveDirectory user can be supplied.

## prerequisites

this module requires you to pass in an IAM role with the following policies
already attached:

  - `AWSDataSyncFullAccess`
  - `AWSDataSyncReadOnlyAccess`

### bucket setup

this assumes destination buckets are managed externally, and avoids changing their
bucket policies to avoid stepping on the toes of any existing tooling. bucket
policies need to allow the following access to the provided role:

```json
{
  "Sid": "DataSyncCreateS3LocationAndTaskAccess",
  "Effect": "Allow",
  "Principal": {
    "AWS": "arn:aws:iam::SOURCE_ACCOUNT_ID:role/ROLE_NAME"
  },
  "Action": [
    "s3:GetBucketLocation",
    "s3:ListBucket",
    "s3:ListBucketMultipartUploads",
    "s3:AbortMultipartUpload",
    "s3:DeleteObject",
    "s3:GetObject",
    "s3:ListMultipartUploadParts",
    "s3:PutObject",
    "s3:GetObjectTagging",
    "s3:PutObjectTagging"
  ],
  "Resource": [
    "arn:aws:s3:::ucsb-lib-example-bucket",
    "arn:aws:s3:::ucsb-lib-example-bucket/*"
  ]
}
```

it's important that buckets are setup with the "**BucketOwnerEnforced**"
ownership control policy (`aws s3api get-bucket-ownership-controls --bucket
"name"`). if this is not in place, objects touched by the datasync process will
be owned by the source account, which will cause unexpected policy/ACL behavior.

if a bucket is using ACLs to grant access (e.g. making the contents public by
giving "read" to "Everyone") you'll need to replace those ACLs with equivalent
policies, e.g.:

```json
{
  "Sid": "",
  "Effect": "Allow",
  "Principal": "*",
  "Action": ["s3:GetObject"],
  "Resource": "arn:aws:s3:::ucsb-lib-example-bucket/*"
}
```

### AD user

an Active Directory user with read access for the filesystem MUST be provided
with the `fsx_ad_domain`, `fsx_ad_user` and `fsx_ad_password` variables. see
https://ucsantabarbara.secretservercloud.com/app/#/secrets/9136

## variables

| name                     |  description                                                                                |
|--------------------------|---------------------------------------------------------------------------------------------|
| datasync_role            | role with `AWSDataSyncFullAccess` and `AWSDataSyncReadOnlyAccess`                           |
| fsx_ad_domain            | the active directory domain for the fxs fs; "library.ucsb.edu"                              |
| fsx_ad_password          | see https://ucsantabarbara.secretservercloud.com/app/#/secrets/9136                         |
| fsx_ad_user              | see https://ucsantabarbara.secretservercloud.com/app/#/secrets/9136                         |
| fsx_filesystem_id        | the id of the filesystem to use; e.g "fs-0123456789123456"                                  |
| fsx_filesystem_path      | subpath to use for the datasync source location; e.g. "/psecial/cylinders"                  |
| fsx_sg_stack             | cloud formation stack tag to match for security group (sorry); e.g. "lib-fsx-1-2"           |
| task_excludes            | path patters to exclude; e.g. "*.bak"                                                       |
| task_includes            | path patterns to include; e.g. "/*" or "/my/content/path/*"                                 |
| task_schedule            | see https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-scheduled-rule-pattern.html |
| target_bucket            | the name of the destination bucket; e.g. "cylinders"                                        |
| uc_protection_level      | used to tag appropriate resources; p1, p2, p3, p4, or p5                                    |

## cost

the cost for the DataSync service is $0.0125/GB.

this module only uses S3's Standard storage class ($0.023/GB in US-West-2; less
UC discounts) for duplicated data. it foregoes verification on existing files,
which should make other S3 costs negligible.

> it's strongly advised to run this within a single region. inter-region data
> transfer costs could be significant increase
