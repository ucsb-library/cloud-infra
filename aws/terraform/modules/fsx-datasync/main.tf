locals {
  target_name = var.target_bucket
}

data "aws_s3_bucket" "target" {
  provider = aws.destination
  bucket   = var.target_bucket
}
