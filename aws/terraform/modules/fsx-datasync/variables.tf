variable "datasync_role" {
  type = string
}

variable "fsx_ad_domain" {
  type = string
}

variable "fsx_ad_password" {
  type = string
}

variable "fsx_ad_user" {
  type = string
}

variable "fsx_filesystem_id" {
  type = string
}

variable "fsx_filesystem_path" {
  type = string
}

variable "fsx_sg_stack" {
  type = string
}

variable "task_excludes" {
  type    = string
  default = ""
}

variable "task_includes" {
  type    = string
  default = "/*"
}

variable "task_schedule" {
  type = string
}

variable "target_bucket" {
  type = string
}

variable "uc_protection_level" {
  type = string
}
