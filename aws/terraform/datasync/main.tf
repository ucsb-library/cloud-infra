locals {
  region = "us-west-2"
}

provider "aws" {
  region = local.region

  default_tags {
    tags = {
      "ucsb:dept:INFR:managed-by" = "terraform"
      "ucsb:dept:INFR:source"     = "https://gitlab.com/ucsb-library/cloud-infra/aws/terraform/datasync"
      "ucsb:availability-level"   = "a1"
      "ucsb:environment"          = "other"
      "ucsb:environment:name"     = "prod-dataflow"
      "ucsb:costing"              = "library-dld"
    }
  }
}

provider "aws" {
  alias  = "its_library"
  region = local.region

  assume_role {
    role_arn = var.source_account_role_arn
  }

  default_tags {
    tags = {
      "ucsb:dept:INFR:managed-by" = "terraform"
      "ucsb:dept:INFR:source"     = "https://gitlab.com/ucsb-library/cloud-infra/aws/terraform/datasync"
      "ucsb:availability-level"   = "a1"
      "ucsb:environment"          = "other"
      "ucsb:environment:name"     = "prod-dataflow"
      "ucsb:service"              = "dld-dataflow"
      "ucsb:costing"              = "library-dld"
    }
  }
}

##
# apply the local `fsx-datasync` module for each configuration object/map in
# `bucket_fs_map`
module "datasync" {
  source   = "../modules/fsx-datasync"
  for_each = var.bucket_fs_map

  providers = {
    aws.destination = aws,
    aws.source      = aws.its_library
  }

  datasync_role       = aws_iam_role.datasync.name
  fsx_filesystem_id   = each.value.fs_id
  fsx_filesystem_path = each.value.path
  fsx_ad_domain       = var.fsx_ad_domain
  fsx_ad_password     = var.fsx_ad_password
  fsx_ad_user         = var.fsx_ad_user
  fsx_sg_stack        = each.value.sg_stack
  task_excludes       = each.value.excludes
  task_includes       = each.value.includes
  task_schedule       = each.value.schedule
  target_bucket       = each.key
  uc_protection_level = each.value.protection_level

  depends_on = [aws_iam_role.datasync]
}

##
# setup a role and trust policy; the module will set this role up with
# appropriate access to the bucket(s) and assume this role when running the task
data "aws_iam_policy_document" "datasync_role_trust" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["datasync.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "datasync" {
  provider           = aws.its_library
  name               = "dld-datasync-fsx-role"
  assume_role_policy = data.aws_iam_policy_document.datasync_role_trust.json
}

resource "aws_iam_role_policy_attachment" "datasync_full" {
  provider   = aws.its_library
  role       = aws_iam_role.datasync.name
  policy_arn = "arn:aws:iam::aws:policy/AWSDataSyncFullAccess"
}

resource "aws_iam_role_policy_attachment" "datasync_read_only" {
  provider   = aws.its_library
  role       = aws_iam_role.datasync.name
  policy_arn = "arn:aws:iam::aws:policy/AWSDataSyncReadOnlyAccess"
}
