# DLD Datasync

configures cross-account synchronization from FSx for Windows in
`its-library-itops` to application-scoped S3 buckets in `library-itops`.

## running

this sets up a role for the datasync access to S3:
`"arn:aws:iam::315384971758:role/dld-datasync-fsx-role". bucket policies need
to be in place allowing:

  - s3:GetBucketLocation
  - s3:ListBucket
  - s3:ListBucketMultipartUploads
  - s3:AbortMultipartUpload
  - s3:DeleteObject
  - s3:GetObject
  - s3:ListMultipartUploadParts
  - s3:PutObject
  - s3:GetObjectTagging
  - s3:PutObjectTagging

see the internal [`fsx-datasync` module][fsx-datasync] documentation for
other important bucket prerequisites. "**BucketOwnerEnforced**" MUST be
set on the destination buckets or unexpected access rules will result.

configure each bucket's source with `bucket_fs_map` in [variables][variables]:

```hcl
cylinders = {
  fs_id            = "fs-01305d409fc367e09",
  path             = "/special/cylinders/",
  sg_stack         = "lib-fsx-1-2",
  schedule         = "cron(0 12 * * ? *)",
  includes         = "/*",
  excludes         = "*.old|*.bak|*.gpk|*.md5|*.txt|*.mem|*.wav|*.wav.*|*.mov|*.mov.*|*.sfl|/avlab/*|/.DS_Store"
  protection_level = "p1"
}
```

plan and apply using the `ucsb-role-terraform-full` role in the `library-itops`
account. this role can assume a role (`DLDTerraform`) with appropriate access to
the `yits-library-itops` account.


[fsx-datasync]: ../modules/fsx-datasync
[variables]: ./variables.tf
