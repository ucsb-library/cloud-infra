variable "bucket_fs_map" {
  type = map(object({
    fs_id            = string
    path             = string
    sg_stack         = string
    schedule         = string
    excludes         = string
    includes         = string
    protection_level = string
  }))

  default = {
    cylinders = {
      fs_id            = "fs-01305d409fc367e09",
      path             = "/special/cylinders/",
      sg_stack         = "lib-fsx-1-2",
      schedule         = "cron(0 12 * * ? *)",
      includes         = "/*",
      excludes         = "*.old|*.bak|*.gpk|*.md5|*.txt|*.mem|*.wav|*.wav.*|*.mov|*.mov.*|*.sfl|/avlab/*|/.DS_Store"
      protection_level = "p1"
    }
    cylinders-dev = {
      fs_id            = "fs-01305d409fc367e09",
      path             = "/special/cylinders/",
      sg_stack         = "lib-fsx-1-2",
      schedule         = "cron(0 12 * * ? *)",
      includes         = "/*",
      excludes         = "*.old|*.bak|*.gpk|*.md5|*.txt|*.mem|*.wav|*.wav.*|*.mov|*.mov.*|*.sfl|/avlab/*|/.DS_Store"
      protection_level = "p1"
    }
  }
}

variable "fsx_ad_domain" {
  type    = string
  default = "library.ucsb.edu"
}

variable "fsx_ad_password" {
  type = string
}

variable "fsx_ad_user" {
  type    = string
  default = "DLDDatasync"
}

variable "source_account_role_arn" {
  type    = string
  default = "arn:aws:iam::315384971758:role/DLDTerraform"
}
