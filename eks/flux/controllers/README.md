## deployed resources

| controller | docs |
| ---------- | ---- |
| [cloudfront](https://github.com/aws-controllers-k8s/cloudfront-controller) | [chart](https://gallery.ecr.aws/aws-controllers-k8s/cloudfront-chart); [iam policy](https://github.com/aws-controllers-k8s/cloudfront-controller/blob/main/config/iam/recommended-policy-arn) |
| [ec2](https://github.com/aws-controllers-k8s/ec2-controller) | [chart](https://gallery.ecr.aws/aws-controllers-k8s/ec2-chart); [iam policy](https://github.com/aws-controllers-k8s/ec2-controller/blob/main/config/iam/recommended-policy-arn) |
| [iam](https://github.com/aws-controllers-k8s/iam-controller) | [chart](https://gallery.ecr.aws/aws-controllers-k8s/iam-chart); [iam policy](https://github.com/aws-controllers-k8s/iam-controller/blob/main/config/iam/recommended-policy-arn) |
| [rds](https://github.com/aws-controllers-k8s/rds-controller) | [chart](https://gallery.ecr.aws/aws-controllers-k8s/rds-chart); [iam policy](https://github.com/aws-controllers-k8s/rds-controller/blob/main/config/iam/recommended-policy-arn) |
| [s3](https://github.com/aws-controllers-k8s/s3-controller) | [chart](https://gallery.ecr.aws/aws-controllers-k8s/s3-chart); [iam policy](https://github.com/aws-controllers-k8s/s3-controller/blob/main/config/iam/recommended-policy-arn) |
| [secrets-manager](https://github.com/aws-controllers-k8s/secretsmanager-controller) | [chart](https://gallery.ecr.aws/aws-controllers-k8s/secretsmanager-chart); [iam policy](https://github.com/aws-controllers-k8s/secretsmanager-controller/blob/main/config/iam/recommended-policy-arn) |

### aws controllers for kubernetes (ack)

#### deploying resources to aws

[api reference](https://aws-controllers-k8s.github.io/community/reference/)

ack controllers are called when their api is declared in a k8s manifest `kind`. some aws resources take several minutes to deploy, so dynamic deployments are not currently supported.

for example, rds endpoint connection information cannot be accessed as a variable in a helm chart and needs to be added as a static value in advance of being deployed.

new aws resources should be deployed using [flux](../../flux).

#### helm chart repository

the ack helm repository requires credentials that are publicly available but must be fetched using the aws cli. these are stored as a secret that is referened by flux.

the secret can be manually updated using the following command:

```
kubectl create secret generic "aws-ecr-authorization" \
  --from-literal=username="AWS" \
  --from-literal=password="$(aws ecr-public get-login-password --region us-east-1)" \
  --namespace flux-system \
  --save-config \
  --dry-run=client \
  -o yaml | \
  kubectl apply -f -
```

#### [iam roles for service accounts (irsa)](https://aws-controllers-k8s.github.io/community/docs/user-docs/irsa/#step-2-create-an-iam-role-and-policy-for-your-service-account)

each ack-controller irsa requires an `assumeRolePolicyDocument` that grants [scoped access](https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRoleWithWebIdentity.html) to their respective APIs. the following json document is exclusive to:
- a unique openID connect (oidc) identity provider in dld's k8s cluster
- service accounts following the naming pattern `ack-*-controller`

```
    {
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "Federated": "arn:aws:iam::406663345920:oidc-provider/oidc.eks.us-west-2.amazonaws.com/id/2726AF38AC3E7F795713F38790102268"
          },
          "Action": "sts:AssumeRoleWithWebIdentity",
          "Condition": {
            "StringLike": {
              "oidc.eks.us-west-2.amazonaws.com/id/2726AF38AC3E7F795713F38790102268:sub": "system:serviceaccount:ack-*-controller:ack-*-controller"
            }
          }
        }
      ]
    }
```
