# [EKS][eks] User Documentation

This is documentation for using the Kubernetes cluster as a developer or DevOps
engineer to deploy application workloads and services.

If you are looking for documentation for maintaining the cluster and its system
workloads, see [Cluster Maintenance][eks-maintenance].

## Accessing the cluster with `kubectl`

If you have an existing `aws-cli` configuration, you can setup `kubectl` access
to the cluster with:

```sh
aws eks --region us-west-2 update-kubeconfig --name dld-eks
```

Your IAM user will need to be added to the cluster’s authorized user `ConfigMap`
as described in [`./eks/maintenance.md`][eks-maintenance-cluster-users]

## Managing Workloads

__Please expand this section._

It's strongly preferred that workloads be managed by an automated process like a
CI/CD pipeline, a GitOps controller (like [flux][fluxcd], and/or an
[operator][k8s-operator]. For each of these use cases, we make extensive use of
[Helm][helm] to templatize deployments and manage releases and rollbacks.

## Deploying Secrets

We manage secrets with [flux][fluxcd] and
[Bitnami Sealed Secrets][sealed-secrets], using patterns
[documented by the `flux` team][flux-sealed-secrets]. Secrets are checked into
this repository at `eks/flux/product-builds/[build-space]/[product]/[secret-name.yaml]`
encrypted with a public key. The private key is held by
`sealed-secrets-controller`, and is used to decrypt the secret for use by
resources within the cluster.

To create a secret, you need `kubeseal` (which can be [installed as documented
in the `sealed-secrets` README][kubeseal-install] and a working config for 
`kubectl` (see above).

Get the public key with:

```sh
kubeseal --fetch-cert --controller-name=sealed-secrets-controller --controller-namespace=sealed-secrets > pub-sealed-secrets.pem
```

Then you can create secrets by making an unencrypted secret file, encrypting it,
and removing the unencrypted version:

```sh
export CLUSTER=dld-eks NAMESPACE=moomin SECRET_NAME=my-secret

kubectl -n $NAMESPACE create secret generic $SECRET_NAME --from-literal=key=value --dry-run=client -o yaml > $SECRET_NAME.yaml
mkdir secrets/$CLUSTER/$NAMESPACE
kubeseal --format=yaml --cert=pub-sealed-secrets.pem < $SECRET_NAME.yaml > secrets/$CLUSTER/$NAMESPACE/$SECRET_NAME-sealed.yaml
rm $SECRET_NAME.yaml
```

This will result in a file like:

```yaml
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  creationTimestamp: null
  name: my-secret
  namespace: moomin
spec:
  encryptedData:
    key: AgCnlOA87np4GPZuf8x6zgCPZC7x6/Zo/3m+4BFRBb/rXvUM2hUexrMxxx2hAqzIFisgsgk7+x3FxkyT8yo8h6UvJsjV75dGMZfvZcQ/MmQlWDBgwcvFRDeSjAChGoQlXLw5Ew4zRN7ZULkrL5j4VnN+Ned1XOlh/kR8QM1xfyvndqAtOnUsHWPsjE3qpok4N029dPuNnuAwN0zESvLSiDU4dLB++aycgNhevBsDxCLSaAQMPmilZ8d8TwyGNkvXr1Vz/ZzGn/c1+nvkeQJRbI+EDIxcCRCZNOTG/bLQedmJN/wJiY6ta2C984cYxplQnK7+t+Sd+2JUH5yXMi7rZqux/bPXWQr0nuaZ2CRzfLt5ACyCfwYkQN/zUWWKduGeOOwtR9qXMmB9wFNGTGp0ytrz4B7GeVHYqkKTFFovTXKoWMVzzmdMM9OBGRzGJhlK+hYRaeoNpUnqThWWy2alNUeSC0oxuerAoCDbQQPixt1gvEV+E99LAZjeygeVxTc6KKq9Ebs/XvqUDU5VcRhkz//HTq+tpvuZ6Xc4+YBCgIV1s5Pv/9BtUP43gszRcaiOs72c8DrGBSBkIR7XYQ6M6MpqwoM+w8LKM05MA4hWXs23cPOOZ7l2G0UPCHOiKaP90c065VRZ25OXJFK5reJ8a9A+9CKDOnm75FRtBleTAfLSqxvXimY1fAvHeN+ZBuHo2rfvsTLQTg==
  template:
    metadata:
      creationTimestamp: null
      name: my-secret
      namespace: moomin
```

When handling secrets in this way, consider the sensitivity of the data you're
handling and how your local system is treating them; e.g. is it stored in
shell history, should this process be done on a secure system, etc...

Once the encrypted secret file exists, you can commit it and create a merge
request. Best practice is to use the commit message to explain the relevance
of the secret and where the data came from for future reference. When it is
merged into `trunk`, flux will roll out the `SealedSecret` resource, which
should have `SYNCED` (boolean) and `STATUS` (an error message, or empty if
synced) variables. `sealed-secrets-controller` will create the secret.

```sh
kubectl -n moomin get sealedsecret my-secret
```

[eks]: https://aws.amazon.com/eks/
[eks-maintenance]: ./eks/maintenance.md
[eks-maintenance-cluster-users]: ./eks/maintenance.md#cluster-users
[fluxcd]: https://fluxcd.io/flux/
[flux-sealed-secrets]: https://fluxcd.io/flux/guides/sealed-secrets/
[helm]: https://helm.sh
[k8s-operator]: https://kubernetes.io/docs/concepts/extend-kubernetes/operator
[kubeseal-install]: https://github.com/bitnami-labs/sealed-secrets/blob/main/README.md#kubeseal
[sealed-secrets]: https://github.com/bitnami-labs/sealed-secrets
