# [EKS][eks] Cluster Maintenance

## `dld-eks` Cluster

### `kube-system` Workloads

The following system workloads may require occassional maintenance:

  - `aws-node`
  - [`aws-node-termination-handler`][aws-node-term] (installed via [Helm][aws-node-term-helm])
  - `coredns`
  - `aws-ebs-csi-driver` (see [__Storage Drivers__][storage-drivers])
    - `ebs-csi-controller`
    - `ebs-csi-node`
    - `ebs-snapshot-controller`
  - `nginx-ingress`
  - `kube-proxy`
  - [`cluster-autoscaler`][k8s-what-is-cluster-autoscaler]
  - `metrics-server`
  - [`aws controllers for kubernetes (ack)`](https://aws-controllers-k8s.github.io/community/docs/community/overview/)
    - [`rds-controller`](ack/rds-install.md)

`kubectl get daemonsets -n kube-system` and
`kubectl get deployments -n kube-system` are useful for identifying any current
system workloads.

### Cluster Users

__Managing these user mappings should be done with care!__

To control access EKS provides integration between AWS IAM and Kubernetes authentication via a
[ConfigMap
`aws-auth`](https://aws.amazon.com/premiumsupport/knowledge-center/eks-api-server-unauthorized-error/).
Many EKS dashboard tasks require the IAM user or role to map to a cluster user
with a range of permissions. These permissions are provided for the IAM role
`ucsb-idp-lib-dld`. Additional user or role mappings can be added at:

```sh
kubectl describe configmap/aws-auth -n kube-system
kubectl edit configmap/aws-auth -n kube-system
```

### Node Groups

`dld-eks` is setup as a multi-purpose cluster serving both long-running production workloads
and ephemeral processes. To support this, it is setup with a number of
[managed node groups][aws-node-groups] configured to provide either On-Demand or Spot EC2
instances as cluster nodes. What follows is a summary of our general provisioning strategy;
the exact and up-to-date node group configuration is available in the
[cluster dashboard][dld-eks-cluster-dash]'s _Configuration_->_Compute_ tab.

The spot node groups seek to cheaply provide consistent resources by supporting a variety
of instance types. These nodes may rotate frequently as spot instance availability changes,
so are best suited to stateless, high-availability, or low-priority workloads. These
instances provide our broadest pool of resources and can also be used to cheaply provide
specialty instance types (e.g. boosting the cluster's overall memory resources with
memory-optimized instances).

On-demand node groups provide stable nodes for workloads that benefit from greater stability.
Since these nodes are provisioned for continuous availability, workloads scheduled to them
should experience less churn. On-demand node groups may also be used with cluster autoscaling
to provide backup capacity when spot instances aren't meeting resource needs.

To support [workload scheduling][k8s-affinity], nodes are labeled with the key
`dld.library.ucsb.edu/availability` and one of the following values:

  - `stable`: for nodes that should be continuously available.
  - `general`: for nodes provisioned on-demand in node groups that may scale in or out relatively frequently.
  - `short-term`: for spot instances (see `SPOT`, below) or nodes that will likely be short-lived for other reasons.

AWS also provides the `eks.amazonaws.com/capacityType` label, with values:

  - `ON_DEMAND`
  - `SPOT`

Handling for AWS instance lifecycle events is provided by
[`aws-node-termination-handler`][aws-node-term].

### Cluster Autoscaler

To keep cluster resources closely aligned with the resource needs of our actual workloads
we have deployed the [Kubernetes Cluster Autoscaler][aws-k8s-cluster-autoscaler]. This
service adjusts the number of nodes running in the node groups, scaling up to ensure pods
can be scheduled and down when resources are idle. It runs as a `Deployment` in the
`kube-system` namespace.

Our configuration uses the `priority` expander to prefer certain node groups when scaling
up. Node group priority is configured by a `ConfigMap` named
[`expander-priority-config`](./expander-priority-config.yaml). The configuration
gives preference to node groups with `as-group`, `spot` or `general` (in that order) in their
names. The autoscaler will estimate needed capacity when pods are unschedulable and take
these priorities into account when determining which and how many nodes to provision. The
rough effect is that auto-provisioned nodes are very likely to be discounted `spot`
instances.

We prevent scale down of `stable` node groups by tightly configuring the group's minumum and
maximum size (the autoscaler won't adjust these variables) and by applying the
`cluster-autoscaler.kubernetes.io/scale-down-disabled=true` annotation to the indivdiual
nodes.

The configuration for the cluster autoscaler itself is maintained at named
[`cluster-autoscaler-autodiscover`](./cluster-autoscaler-autodiscover.yaml).
`kubectl -n kube-system get deployment.apps/cluster-autoscaler -o yaml` will output the
current autoscaler configuration details.

#### Resource Requests & Limits

Successfully scheduling workloads and scaling cluster capacity requires that workloads
accurately communicate their resource needs. Therefore each pod should specify
[resource requests and limits][k8s-resource] for its containers. Failing to do this,
particularly for workloads with significant memory needs, often leads to the scheduler
mismanaging memory resources for a given node, causing node availablity issues.

To avoid accidental failure to specify resource requests and limits, we set [LimitRange] [k8s-limit-range] resources on various namespaces to provide default resource requests
and limits for pods in those namespaces. Note that this is intended as a fallback, and
individual engineers are still responsible for setting requests and limits on resources
deployed from their projects. An example limit range file is included at
[`limit-range.yaml`](.limit-range.yaml).

#### Overprovisioning

Provisioning new nodes in response to unschedulable pods takes a few minutes. To
avoid wait times in the case of scale up, we "overprovision" by deploying low-priority
`pause` workloads in a variety of sizes. If the cluster becomes resource constrained,
these pods will be preempted to make space for real work. The cluster will then scale
up to make space for the evicted `overprovisioning` pods.

To scale up the overprovisioned resources, add a new `Deployment` using the template at
[`./overprovision.yaml`](`./overprovision.yaml`). Provide a unique `metadata.name`, change
the `spec.template.spec.containers.resources.cpu` and/or
`spec.template.spec.containers.resources.memory`, and set the scale at `spec.replicas`.

List current `overprovisioning` pods with:
`kubectl get pods -n kube-system -l run=overprovisioning`

### [Priority Classes][k8s-pod-priority]

Kubernetes allows configuration of `PriorityClass` resources, which can be
applied to pods to ensure important workloads are given priority in scheduling.

The [priority class documentation][k8s-pod-priority] provides a full description
of the behavior involved, but the simplified version is that a `PriorityClass`
is a `metadata.name` and a `value`. The value is a number up to 1 billion(!);
the higher the number, the higher the priority of the resources in the class.
Our classes space the numbers out quite a bit to leave lots of space for new
classes.

The other major variable is `preemptionPolicy`. This controls whether an
unscheduled pod will evict an existing workload. The default is that a pod will
preempt an existing pod with lower priority than itself. A few of our classes
use the `preemptionPolicy: Never` configuration, ensuring that they don't evict
running workloads. This is useful especially for review and staging apps, which
benefit from some stability, but shouldn't halt other work in order to get
scheduled (it's really best if these apps reveal any cluster load issues
eagerly).

`kubectl get priorityclass` will list existing priority classes.

The configuration for the cluster's classes is maintained at
[`./priority_classes.yaml`](./priority_classes.yaml).
`kubctl apply -f priority_classes.yaml` will update the cluster to the current
configuration.

### Cluster Upgrades

Regular cluster upgrades are advisable to keep up with the latest EKS features,
security and bug fixes, and changes in Kubernetes APIs.

Doing this involves three steps:

  1. ensure all nodes are updated to the lastest version;
  1. upgrade the cluster control plane;
  1. upgrade images `kube-system` workloads.

The first two of these can be done from the EKS dashboard at the push of a
button, but need to be handled with some care.

#### Upgrading Node Groups

Node upgrades become available somewhat regularly, and can be run from the
dashboard for each active node group. It's best to do this regularly rather
than waiting for a cluster upgrade to become available. This process takes
some time, and will result in churn for the active nodes in the group. Workloads
will be rescheduled to other nodes, so this should be undertaken thoughtfully,
particularly for `stable` node groups.

You can see the current node versions with `kubectl get nodes`. Compare with
`kubectl version --short | awk 'FNR == 2 { print $3 }'`. If these two match,
it should be okay to move forward with the cluster upgrade.

![EKS dashboard with pending node group upgrades](../assets/eks-node-up.png)

Once you push the button, EKS will try to drain the nodes in accordance with
scheduling rules to process the upgrades. Sometimes it will fail to do this and
will leave the group partially upgraded. This is fine, and you can and should
push the button again. If a node group is failing to upgrade consistently, some
diagnosis of the workloads on the relevant nodes may be called for. Running
these upgrades eagerly will leave a lot of time to look into potential issues
before they become time sensitive.

#### Upgrading the Cluster (control plane)

Before you start, review [EKS: Updating a cluster][eks-update]. That guide is
updated with version specific upgrade information and other details that might
be new since this writing.

If you have cleared any version specific prerequisites, you can upgrade the
cluster from the management dashboard. This, again, will take some time.
Unlike the node upgrades, past experience is that the cluster upgrade is
without complications.

You can only upgrade one version at a time. If the cluster has fallen several
versions behind, it's best to complete the next step and go through this whole
process for each version, rather than try to repeatedly upgrade.

#### Updating Images for `kube-system` Workloads

Once the cluster is up-to-date, you'll want to ensure the latest supported image
versions are deployed for [system level workloads](#kube-system-workloads).
[EKS: Updating a cluster][eks-update] may have details about the secific versions
of `kube-proxy`, `coredns`, and `aws-node` that should be running. For other
workloads, you may need to refer to upstream documentation to identify pressing
upgrades.

### Ingress

External access to cluster resources via HTTP(S) is managed by
[`Ingress`][k8s-ingress]. Our cluster is configured to support a single
`IngressClass` (`nginx`). Ingress objects of this class are handled by the
[NGINX ingress controller][k8s-nginx-ingress].

This controller is deployed via `Helm` in the `ingress-nginx` namespace:

```sh
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --values ./eks/nginx-ingress.yaml --create-namespace
```

Updates should be run periodically from this chart, particularly when
`networking.k8s.io` APIs are changing.

HTTP traffic to the cluster is routed through an AWS "Classic Load Balancer",
tagged `kubernetes.io/service-name: ingress-nginx/ingress-nginx-controller`.
This, in turn, routes to the `ingress-nginx-controller-admission` `ClusterIP`
service. Individual `nginx-ingress-controller` pods route traffic to the other
services running in the cluster.

The effect is that DNS names pointing to the AWS Load Balancer will be routed
according to the ingress rules. e.g. if an `Ingress` exists with `host:
example.dld.library.ucsb.edu`, the cluster will route traffic from that address to
the designated backend service. Traffic from hosts with no associated `Ingress`
are routed to `ingress-nginx-defaultbackend` and should respond "default backend - 404".

### TLS Certificates

Provisioning for certificates is handled by [`cert-manager`][cert-manager].
`cert-manager` includes a number of custom resource definitons for Kubernetes
allowing creation of certificates and certificate issuers. Issuers can be setup
with [various configurations][cert-manager-issuers], including ACME. Where
appropriate `cert-manager` automates verification with `solvers` configured on
the issuer.


Our current issuer configurations are maintained in
[`cert-manager-issuers.yaml`](./cert-manager-issuers.yaml). For testing
purposes, we have a `letsencrypt-staging` issuer, which issues invalid
certificates from LetsEncrypt's `acme-staging` API.

#### `cert-manager` Helm

The `cert-manager` deployment is managed by `helm` in the `cert-manager`
namespace. The chart used is `jetstack/cert-manager`. It was originally
deployed from `v1.4.0` with the default values.

### Storage Drivers

Kubernetes supports provisioning and managing storage with a [`Container Storage Interface`][k8s-csi-drivers]. Individual storage drivers can be deployed and linked to
a `StorageClass`, allowing pods to create, attach, detach, snapshot, delete,
etc... storage according to their needs.

You can list the available storage classes with `kubectl get storageclass`.

When we first deployed `dld-eks` the recommended approach for handing storage in EKS
was a built-in driver providing an [EBS `gp2`][aws-ebs-volume-types] storage
class. This driver has since been deemphasized by Amazon and is slated for
deprecation. It is now referred to in documentation as the "in-tree" storage
provider. The replacement for EBS volumes is the
[`aws-ebs-csi-driver`][ebs-volume-driver].

While many existing persistent volumes are deployed with the older `gp2` storage
class, we are exploring the newer driver. It is configured to provide the
`ebs-sc` storage class, which is the cluster default for new volumes with
an unspecified class.

#### `ebs-csi` Driver Helm

The EBS driver is deployed with `helm` in the `kube-system` namespace. For
consistency, it is configured to add AWS tags to volumes in the same way as the
in-tree driver. The values file for the helm deployment is at
[`./ebs-driver.yaml`](./ebs-driver.yaml).  To deploy:

```
helm repo add aws-ebs-csi-driver https://kubernetes-sigs.github.io/aws-ebs-csi-driver
helm repo update
helm upgrade --install aws-ebs-csi-driver --namespace kube-system aws-ebs-csi-driver/aws-ebs-csi-driver -f ./ebs-driver.yaml
```

[aws-ebs-volume-types]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volume-types.html
[aws-k8s-cluster-autoscaler]: https://docs.aws.amazon.com/eks/latest/userguide/cluster-autoscaler.html
[aws-node-groups]: https://docs.aws.amazon.com/eks/latest/userguide/managed-node-groups.html
[aws-node-term]: https://github.com/aws/aws-node-termination-handler
[aws-node-term-helm]: https://github.com/aws/aws-node-termination-handler#khelm
[cert-manager]: https://cert-manager.io/docs/
[cert-manager-issuers]: https://cert-manager.io/docs/configuration/
[dld-eks-cluster-dash]: https://us-west-2.console.aws.amazon.com/eks/home?region=us-west-2#/clusters/dld-eks
[ebs-volume-driver]: https://github.com/kubernetes-sigs/aws-ebs-csi-driver
[eks]: https://aws.amazon.com/eks/
[eks-update]: https://docs.aws.amazon.com/eks/latest/userguide/update-cluster.html
[k8s-affinity]: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/
[k8s-csi-drivers]: https://kubernetes-csi.github.io/docs
[k8s-ingress]: https://kubernetes.io/docs/concepts/services-networking/ingress/
[k8s-limit-range]: https://kubernetes.io/docs/concepts/policy/limit-range/
[k8s-nginx-ingress]: https://kubernetes.github.io/ingress-nginx/
[k8s-pod-priority]: https://kubernetes.io/docs/concepts/scheduling-eviction/pod-priority-preemption/
[k8s-resource]: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#requests-and-limits
[k8s-what-is-cluster-autoscaler]: https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/FAQ.md#what-is-cluster-autoscaler
