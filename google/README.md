# Google services

A number of our applications use Google for sending mail, authentication, or
both.  These are managed from the [GCP console](https://console.cloud.google.com/).

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Google services](#google-services)
- [Mailing](#mailing)
    - [Starlight and Shoreline](#starlight-and-shoreline)
    - [ADRL](#adrl)
- [Authentication](#authentication)
    - [Starlight and Shoreline](#starlight-and-shoreline-1)

<!-- markdown-toc end -->

# Mailing

## Starlight and Shoreline

For sending mail, Starlight and Shoreline both use SMTP via instance-specific
Google accounts:

- [Starlight (production)](https://epm.library.ucsb.edu/app/#/secret/2893/general)
- [Starlight (staging)](https://epm.library.ucsb.edu/app/#/secret/2894/general)
- [Shoreline (production)](https://epm.library.ucsb.edu/app/#/secret/2895/general)
- [Shoreline (staging)](https://epm.library.ucsb.edu/app/#/secret/2896/general)

These are first-class accounts provisioned by ETS (what campus documentation
calls [“functional
accounts”](https://security.ucsb.edu/it-security-resources/sharing-protected-information-google))
as opposed to [service
accounts](https://cloud.google.com/iam/docs/understanding-service-accounts).
Their credentials (from Secret Server above) are used directly in applications’
`SMTP_USERNAME` and `SMTP_PASSWORD` environment variables.

## ADRL

Being a legacy application, ADRL’s mailing system is configured differently.
The `adrl@library.ucsb.edu` address is not associated with a functional account,
but rather a [Google
group](https://groups.google.com/a/library.ucsb.edu/g/adrl).  When the ADRL
`ContactUsMailer` sends an email to `adrl@library.ucsb.edu`, it is routed
through the campus Connect SMTP relay, and posted to the Google group.

ADRL is doubly a special case because its email settings are not configured in
the application itself, except for the SMTP port.  Rather, the actual mail
processing is handled by postfix, which runs on the same server.  Postfix also
has no special configuration: when ADRL attempts to send an email to
`adrl@library.ucsb.edu`, postfix queries the `MX` record of `library.ucsb.edu`.
Normally this would be a `google.com` domain, but ADRL uses the Windows DNS
instead of the `bind` servers that most library services use.  Windows DNS
assigns the campus Connect SMTP relay to the `library.ucsb.edu` `MX` record,
which accepts the email from ADRL and posts it to the Google group without
requiring further authentication.

# Authentication

ADRL does not use Google for authentication; it uses both campus LDAP and the
library Active Directory for public vs. staff login.

Shoreline currently has no special use cases for authentication, and so uses the
default Rails user auth.

## Starlight and Comet

Starlight and Comet handle authentication with Google’s OAuth credential API.
Authorized Connect users can manage the API accounts on the Google Cloud console:

- [starlight-stage](https://console.cloud.google.com/apis/credentials?project=starlight-stage)
  ([Secret Server entry](https://epm.library.ucsb.edu/app/#/secret/3130/general)
- [starlight-prod](https://console.cloud.google.com/apis/credentials?project=starlight-prod)
  ([Secret Server entry](https://epm.library.ucsb.edu/app/#/secret/3178/general)
- [comet-staging](https://console.cloud.google.com/apis/credentials?project=comet-staging-312023)
  ([Secret Server entry](https://epm.library.ucsb.edu/app/#/secret/3180/general)
- [comet-prod](https://console.cloud.google.com/apis/credentials?project=comet-prod-312117)
  ([Secret Server entry](https://epm.library.ucsb.edu/app/#/secret/3179/general)

Setting up a new application requires configuring the OAuth consent screen, then
creating a new OAuth Client ID, with the “Authorized Redirect URI” set to the
application’s callback URL.
